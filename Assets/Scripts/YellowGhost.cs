﻿using UnityEngine;
using System.Collections;

public class YellowGhost : Ghost {

	// Use this for initialization
	protected override void Awake () {
		base.Awake ();
		score = 5;
		animationSpeed=1f;
	}

	protected override void Start() {
		base.Start ();
		this.GetComponent<Renderer>().material.color = new Color(1,1,0);
	}

	protected override void OnMouseDown() {
		base.OnMouseDown();
		holder.GetComponent<Animator>().speed=0;
		holder.GetComponent<Animator>().StopPlayback();
	}
}
