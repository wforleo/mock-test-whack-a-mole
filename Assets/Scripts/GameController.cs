﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject[] spawnPoints; 
	public GameObject ghostPrefab;

	public Text scoreText;
	public Text timeLeftText;
	public Text highScoreText;

	public CanvasGroup myGroup;

	public int Score {get; set;}
	public int HighScore {get; set;}
	public int TimeLeft {get; set;}

	public bool restart = false;

	void Awake() {
		Score = 0;
		TimeLeft = 30;
		HighScore=0;
	}
	
	void Start () {
		startGame ();
	}

	void Update() {
		if (restart) {
			if (GameObject.FindObjectOfType<Ghost> () == null) {
				if (Score > HighScore)
					HighScore = Score;

				highScoreText.text = "HighScore: " + HighScore;
				myGroup.interactable = true;
				myGroup.alpha=1;
				}
			}
		}


	public void addNewGhost() {

		/*pseudo code
		 * instantiate a ghost prefab
		 * pick a random number 0,1,2
		 * use a switch to add the Component to the Ghost object - remembering it is the child object that we need to add it to 
		 */

			GameObject ghost = Instantiate(ghostPrefab) as GameObject;
			switch(Random.Range(0,3)){
			case 0:
				ghost.transform.Find("Ghost").gameObject.AddComponent<BlueGhost>();
				break;
			case 1:
				ghost.transform.Find("Ghost").gameObject.AddComponent<RedGhost>();
				break;
			case 2:
				ghost.transform.Find("Ghost").gameObject.AddComponent<YellowGhost>();
				break;
			}

			int spawnNumber = Random.Range(0,9);
			ghost.transform.position = spawnPoints[spawnNumber].transform.position;

			 

	}

	private void doEverySecond() {
		/*pseudo code
		 * call addNewGhost();
		 * change time value
		 * check when time is 0
		 * if time isn't 0 then need to call Invoke("doEverySecond", 1.0f); again
		 */
		TimeLeft--;
		timeLeftText.text = "TimeLeft: " + TimeLeft;

		if(TimeLeft > 0) Invoke("doEverySecond", 1.0f);
		else restart = true;
		 
		addNewGhost(); 



	}



	public void startGame(){

		myGroup.interactable = false;
		myGroup.alpha=0;
		restart = false;
		Score = 0;
		TimeLeft = 30;
		scoreText.text = "Score: " + Score;
		timeLeftText.text = "TimeLeft: " + TimeLeft;
		Invoke("doEverySecond", 1.0f);

	}

	public void incrementScore(int score) {
		Score += score;
		scoreText.text = "Score: " + Score;
	}

}
